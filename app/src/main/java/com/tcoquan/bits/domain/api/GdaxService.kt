package com.tcoquan.bits.domain.api

import com.tcoquan.bits.domain.api.model.Subscribe
import com.tcoquan.bits.domain.api.model.Ticker
import com.tinder.scarlet.WebSocket
import com.tinder.scarlet.ws.Receive
import com.tinder.scarlet.ws.Send
import io.reactivex.Flowable

interface GdaxService {
    @Receive
    fun observeWebSocketEvent(): Flowable<WebSocket.Event>

    @Send
    fun sendSubscribe(subscribe: Subscribe)

    @Receive
    fun observeTicker(): Flowable<Ticker>
}