package com.tcoquan.bits.ui

import androidx.lifecycle.ViewModel
import com.tcoquan.bits.domain.api.GdaxService
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel
@Inject constructor(
    gdaxService: GdaxService
) : ViewModel() {


}