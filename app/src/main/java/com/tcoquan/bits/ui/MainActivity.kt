package com.tcoquan.bits.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.tcoquan.bits.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    companion object {
        private const val VIDEO_URL =
            "https://stream.mux.com/02cDqggWRQ2GkJskapoOW2OZq7I4NGL2y8aFApetXkBA.m3u8"
    }

    lateinit var binding: ActivityMainBinding
    lateinit var viewModel: MainViewModel
    lateinit var chatAdapter: ChatAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        binding.btnClose.setOnClickListener { finish() }
        binding.btnMenu.setOnClickListener {
            Toast.makeText(baseContext, "Not implemented", Toast.LENGTH_SHORT).show()
        }

        initVideoView()
        initChat()
        initAuction()
    }

    private fun initVideoView() {
        binding.progressBar.visibility = View.VISIBLE

        try {
            val video: Uri = Uri.parse(VIDEO_URL)
            binding.videoView.setMediaController(null)
            binding.videoView.setVideoURI(video)
        } catch (e: Exception) {
            Log.e("Error", e.message ?: "")
            e.printStackTrace()
        }
        binding.videoView.requestFocus()
        binding.videoView.setOnPreparedListener {
            binding.progressBar.visibility = View.GONE
            binding.videoView.start()
        }

        binding.videoView.setOnLongClickListener {
            val intent = Intent(baseContext, FullScreenActivity::class.java)
            startActivity(intent)
            true
        }
    }

    private fun initChat() {
        chatAdapter = ChatAdapter()
        binding.rvChat.layoutManager =
            LinearLayoutManager(baseContext, LinearLayoutManager.VERTICAL, true)
        binding.rvChat.adapter = chatAdapter

        binding.btnSend.setOnClickListener {
            chatAdapter.addMessage(binding.edtChat.text.toString())
            binding.edtChat.text.clear()
            binding.rvChat.smoothScrollToPosition(0)
        }
    }

    private fun initAuction() {
        binding.txtPropositionPrice1.setOnClickListener {
            binding.txtActualPrice.text = binding.txtPropositionPrice1.text
            binding.txtPropositionPrice1.text = binding.txtPropositionPrice2.text
            binding.txtPropositionPrice2.text =
                (binding.txtPropositionPrice2.text.removeSuffix("€").toString()
                    .toInt() + 2).toString() + "€"
        }

        binding.txtPropositionPrice2.setOnClickListener {
            binding.txtActualPrice.text = binding.txtPropositionPrice2.text
            binding.txtPropositionPrice1.text =
                (binding.txtPropositionPrice2.text.removeSuffix("€").toString()
                    .toInt() + 2).toString() + "€"
            binding.txtPropositionPrice2.text =
                (binding.txtPropositionPrice2.text.removeSuffix("€").toString()
                    .toInt() + 4).toString() + "€"
        }

        binding.txtPropositionPriceOther.setOnClickListener {
            Toast.makeText(baseContext, "Not implemented", Toast.LENGTH_SHORT).show()
        }
    }
}