package com.tcoquan.bits.ui

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.tcoquan.bits.databinding.ActivityFullScreenBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FullScreenActivity : AppCompatActivity() {

    companion object {
        private const val VIDEO_URL =
            "https://stream.mux.com/02cDqggWRQ2GkJskapoOW2OZq7I4NGL2y8aFApetXkBA.m3u8"
    }

    lateinit var binding: ActivityFullScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityFullScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initVideoView()
    }

    private fun initVideoView() {
        binding.progressBar.visibility = View.VISIBLE
        try {
            val video: Uri = Uri.parse(VIDEO_URL)
            binding.videoView.setMediaController(null)
            binding.videoView.setVideoURI(video)
        } catch (e: Exception) {
            Log.e("Error", e.message ?: "")
            e.printStackTrace()
        }
        binding.videoView.requestFocus()
        binding.videoView.setOnPreparedListener {
            binding.progressBar.visibility = View.GONE
            binding.videoView.start()
        }
        binding.videoView.setOnCompletionListener { finish() }

        binding.videoView.setOnClickListener {
            finish()
        }
    }
}