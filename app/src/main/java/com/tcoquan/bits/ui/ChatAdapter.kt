package com.tcoquan.bits.ui

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tcoquan.bits.databinding.ItemChatMessageBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ChatAdapter : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {

    private var messages = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemChatMessageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(messages[position])
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setDatas(messages: List<String>) {
        this.messages.clear()
        this.messages.addAll(messages)
        CoroutineScope(Dispatchers.Main)
            .launch {
                notifyDataSetChanged()
            }
    }

    fun addMessage(message: String) {
        this.messages.add(0,message)
        CoroutineScope(Dispatchers.Main)
            .launch {
                //TODO Use better ...
                notifyDataSetChanged()
            }
    }

    inner class ViewHolder(private val binding: ItemChatMessageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(message: String) {
            binding.txtMessage.text = message
        }
    }
}